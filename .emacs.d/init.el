;; ~/.emacs.d/site-lisp 以下全部読み込み
(let ((default-directory (expand-file-name "~/.emacs.d/site-lisp")))
  (add-to-list 'load-path default-directory)
  (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
      (normal-top-level-add-subdirs-to-load-path)))

(require 'init-loader)
(setq init-loader-show-log-after-init nil)
(init-loader-load "~/.emacs.d/conf")

;; cl-libパッケージを読み込む
(require 'cl-lib)
;; スタートアップメッセージを非表示
(setq inhibit-startup-screen t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   (quote
    ("/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/202006.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200602.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200605.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200606.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200612.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200616.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200617.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200618.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200619.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200623.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200626.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200630.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200701.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200706.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200707.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200708.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200709.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200710.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200713.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200714.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200715.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200717.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200718.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200720.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200721.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200722.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200727.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200728.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200729.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200730.org" "/Users/tsuchinoko/Storage/OneDrive/Org/10_Journal/20200731.org")))
 '(package-selected-packages
   (quote
    (org-attach-screenshot exec-path-from-shell flycheck org-download org-journal plantuml-mode solarized-theme yaml-mode scss-mode markdown-mode scala-mode rust-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; 更新されたファイルを自動的に読み込み直す
(global-auto-revert-mode t)
(put 'downcase-region 'disabled nil)
