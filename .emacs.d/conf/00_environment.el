;; 日本語環境の設定
(set-language-environment "Japanese")
(prefer-coding-system 'utf-8)
(set-default 'buffer-file-coding-system 'utf-8)

;; ターミナル以外はツールバー、スクロールバーを非表示
(when window-system
  ;; tool-barを非表示
  (tool-bar-mode 0)
  ;; scroll-barを非表示
  (scroll-bar-mode 0))

;; フォントの行間を変更
(setq-default line-spacing 0.2)

;;; 全角スペースや改行などを可視化する
(setq whitespace-display-mappings
      '(
        (space-mark ?\x3000 [?\□]) ; zenkaku space
        (newline-mark 10 [182 10]) ; ¶
        (tab-mark 9 [187 9] [92 9]) ; tab » 187
        ))

(setq whitespace-style
      '(
        spaces
        trailing
        newline
        space-mark
        tab-mark
        newline-mark))

;; display zenkaku space
(setq whitespace-space-regexp "\\(\u3000+\\)")

(global-whitespace-mode t)
(define-key global-map (kbd "<f5>") 'global-whitespace-mode)
(set-face-foreground 'whitespace-newline "Gray")

;;; 保存時に末尾空白を削除する
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; 警告音もフラッシュも全て無効(警告音が完全に鳴らなくなるので注意)
(setq ring-bell-function 'ignore)

;; PATH を引き継ぐ
(exec-path-from-shell-initialize)
