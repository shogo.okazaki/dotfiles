;;-------------;;
;; org-mode    ;;
;;-------------;;
;; ファイルの場所
(setq org-directory "~/Storage/OneDrive/Org")

;; 画像をインラインで表示
;; (setq org-startup-with-inline-images t)

;; 見出しの余分な*を消す
(setq org-hide-leading-stars t)

;; LOGBOOK drawerに時間を格納する
(setq org-clock-into-drawer t)

;; .orgファイルは自動的にorg-mode
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

;; 折返しを全体で有効にする
(setq org-startup-truncated nil)

;; 画像をスクリーンショットでキャプチャ
(require 'org-attach-screenshot)

;; TODO状態
(setq org-todo-keywords
      '((sequence "TODO(t)" "WAIT(w)" "NOTE(n)"  "|" "DONE(d)" "SOMEDAY(s)" "CANCEL(c)")))
;; DONEの時刻を記録
(setq org-log-done 'time)

;; ショートカットキー
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cj" 'org-journal-new-entry)

;; メモを一発で見るための設定
;; https://qiita.com/takaxp/items/0b717ad1d0488b74429d から拝借
(defun show-org-buffer (file)
  "Show an org-file FILE on the current buffer."
  (interactive)
  (if (get-buffer file)
      (let ((buffer (get-buffer file)))
        (switch-to-buffer buffer)
        (message "%s" file))
    (find-file (concat "~/Storage/OneDrive/Org/00_Inbox/" file))))
(global-set-key (kbd "C-M-^") '(lambda () (interactive) (show-org-buffer "note.org")))

;; アジェンダ表示の設定
;; アジェンダ表示の対象ファイル
(setq org-agenda-files '(
			 "~/Storage/OneDrive/Org/00_Inbox/todo.org"
			 "~/Storage/OneDrive/Org/10_Journal"
			 ))

;; plantuml.jarへのパスを設定
(setq org-plantuml-jar-path "/usr/local/Cellar/plantuml/1.2020.10/libexec/plantuml.jar")
;; org-babelで使用する言語を登録
(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t)))

;; org-journal の設定
(setq org-journal-dir "~/Storage/OneDrive/Org/10_Journal/")
(setq org-journal-date-format "%Y-%m-%d (%a)")
(setq org-journal-time-format "<%Y-%m-%d %R>\n ")
(setq org-journal-file-format "%Y%m%d.org")

;; 日ごとにファイルを作成したい
;; (setq org-journal-file-type 'dayly)
(require 'org-journal)

;; Org-captureの設定
;; Org-captureのテンプレート（メニュー）の設定
(setq org-capture-templates
      '(("c"
	 "Note: 即開くメモ"
	 entry
	 (file "~/Storage/OneDrive/Org/00_Inbox/note.org")
	 "* %?\n Entered on %U"
	 :empty-lines 1)

	("t"
	 "TODO：思いついたタスクを放り込んでおく"
	 entry
	 (file+headline "~/Storage/OneDrive/Org/00_Inbox/todo.org" "INBOX")
	 "** TODO %?%T\n"
	 :empty-lines 1)

	("m"
	 "Meeting：会議録メモ"
	 entry
	 (file+datetree "~/Storage/OneDrive/Org/00_Inbox/meeting.org")
	 "* %?\n Entered on %U"
	 :empty-lines 1
	 :jump-to-captured 1)
	)

      )
