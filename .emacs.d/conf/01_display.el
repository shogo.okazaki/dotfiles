;;; 現在行のハイライトg
;; (defface my-hl-line-face
;;   ;; 背景がdarkならば背景色を紺に
;;   '((((class color) (background dark))
;;      (:background "NavyBlue" t))
;;     ;; 背景がlightならば背景色を青に
;;     (((class color) (background light))
;;      (:background "LightSkyBlue" t))
;;     (t (:bold t)))
;;   "hl-line's my face")
;; (setq hl-line-face 'my-hl-line-face)
(global-hl-line-mode t)

;; paren-mode：対応する括弧を強調して表示する
(setq show-paren-delay 0) ; 表示までの秒数。初期値は0.125
(show-paren-mode t) ; 有効化
;; parenのスタイル: expressionは括弧内も強調表示
(setq show-paren-style 'expression)

;; フォントの設定
;; Asciiフォントを Ricty Discord に
(set-face-attribute 'default nil
                    :family "Ricty Discord"
                    :height 140)

;; 日本語フォントを Hiragino に
(set-fontset-font
  nil 'japanese-jisx0208
  (font-spec :family "Hiragino Sans W3"
             :height 140))
