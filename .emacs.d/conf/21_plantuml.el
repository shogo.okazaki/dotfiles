;; PlantUML Mode の設定
(el-get-bundle plantuml-mode
	       :type github
	       :name plantuml-mode
	       :pkgname "skuro/plantuml-mode"
	       :branch "master")
(setq plantuml-jar-path "/usr/local/Cellar/plantuml/1.2020.10/libexec/plantuml.jar")
;; Enable puml-mode for PlantUML files
(add-to-list 'auto-mode-alist '("\.pu$'" . plantuml-mode))
;;(setq plantuml-output-type "svg")
(setq plantuml-options "-charset UTF-8")
