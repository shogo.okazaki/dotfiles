;; カラム番号も表示
(column-number-mode t)

;; ファイルサイズ表示
(size-indication-mode t)

;; 時計の表示
(setq display-time-day-and-date t)
(setq display-time-24hr-format t)
(display-time-mode t)

;; タイトルバーにフルパスを表示
(setq frame-title-format "%f")

;; 行番号を常に表示
(global-linum-mode t)
