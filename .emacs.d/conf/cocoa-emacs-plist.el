;; CocoaEmacs以外はメニューバーを非表示
(unless (eq window-system 'ns)
  ;; menu-barを非表示
  (menu-bar-mode 0))

;; 初期起動時の大きさ指定
(setq initial-frame-alist
      (append (list
	       '(width . 90)
	       '(height . 56)
	       )
	      initial-frame-alist))
(setq default-frame-alist initial-frame-alist)

;;-------------------------------
;; solarized テーマを使う
;;-------------------------------
;; fringeを背景から目立たせる
;; (setq solarized-distinct-fringe-background t)

;; mode-line を目立たせる
(setq solarized-high-contrast-mode-line t)
;;
;; bold 度合いを減らす
;; (setq solarized-use-less-bold t)
;;
;; italic を増やす
;; (setq solarized-use-more-italic t)
;;
;; インジゲータの色を減らす
;; (setq solarized-emphasize-indicators nil)

;; org の見出し行の文字の大きさを変えない
;; (setq solarized-scale-org-headlines nil)
;;
;; フォントサイズを変更しない
;; (setq solarized-height-minus-1 1)
;; (setq solarized-height-plus-1 1)
;; (setq solarized-height-plus-2 1)
;; (setq solarized-height-plus-3 1)
;; (setq solarized-height-plus-4 1)

;;(load-theme 'solarized-light t)
(load-theme 'solarized-dark t)
