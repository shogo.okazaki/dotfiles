;;; -*- no-byte-compile: t -*-
(define-package "rust-mode" "20200513.812" "A major emacs mode for editing Rust source code" '((emacs "25.1")) :commit "1603a258d1dec3c5f8671a12782d913bb828864a" :keywords '("languages") :authors '(("Mozilla")) :maintainer '("Mozilla") :url "https://github.com/rust-lang/rust-mode")
